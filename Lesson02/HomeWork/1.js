
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
        + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
        и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
        Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
 var buttonContainer = document.getElementById('buttonContainer');
 button1 = buttonContainer.querySelector('.showButton[data-tab="1"]');
 button2 = buttonContainer.querySelector('.showButton[data-tab="2"]');
 button3 = buttonContainer.querySelector('.showButton[data-tab="3"]');


 button1.onclick = function () {
   var tabContainer = document.getElementById('tabContainer');
       firstContainer = tabContainer.querySelector('.tab[data-tab="1"]');
       firstContainer.classList.add('active');

   var prevActive2 = document.getElementById('tabContainer').querySelector('.tab[data-tab="2"]');
       prevActive2.classList.remove('active');
   var prevActive3 = document.getElementById('tabContainer').querySelector('.tab[data-tab="3"]');
       prevActive3.classList.remove('active');

     
 }

 button2.onclick = function () {
   var tabContainer = document.getElementById('tabContainer');
       secondContainer = tabContainer.querySelector('.tab[data-tab="2"]');
       secondContainer.classList.add('active');

   var prevActive1 = document.getElementById('tabContainer').querySelector('.tab[data-tab="1"]');
       prevActive1.classList.remove('active');
   var prevActive3 = document.getElementById('tabContainer').querySelector('.tab[data-tab="3"]');
       prevActive3.classList.remove('active');
  
 }

 button3.onclick = function () {
   var tabContainer = document.getElementById('tabContainer');
       thirdContainer = tabContainer.querySelector('.tab[data-tab="3"]');
       thirdContainer.classList.add('active');

   var prevActive1 = document.getElementById('tabContainer').querySelector('.tab[data-tab="1"]');
       prevActive1.classList.remove('active');
   var prevActive2 = document.getElementById('tabContainer').querySelector('.tab[data-tab="2"]');
       prevActive2.classList.remove('active');
    
 }
 
 var hide = document.getElementById('tabContainer');
 hide.onclick = function hideAllTabs () {
   var deleteActive = document.getElementById('tabContainer').querySelector('.active');
       deleteActive.classList.remove('active');
 }

 
